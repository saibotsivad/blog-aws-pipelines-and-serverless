# blog-aws-pipelines-and-serverless

Example repository demonstrating using Bitbucket Pipelines
and serverless to automate testing on pull requests and
merges, and deployments on merges. See the blog post for
more details:

https://davistobias.com/articles/aws-pipelines-and-serverless/

## Project setup:

Clone the repository to your local machine:

```bash
git clone https://saibotsivad@bitbucket.org/saibotsivad/blog-aws-pipelines-and-serverless.git
cd blog-aws-pipelines-and-serverless
```

Install all dependencies:

```bash
npm install
```

Make sure tests pass:

```bash
npm run test
```

## License

The contents of this repository are published under the
[Very Open License](http://veryopenlicense.com).
