module.exports.api = async () => {
	return {
		statusCode: '200',
		body: '<h1>This is a demo server!</h1><p>Check out <a href="https://davistobias.com/articles/aws-pipelines-and-serverless/">the blog post</a> for more information.',
		headers: {
			'Content-Type': 'text/html'
		}
	}
}
