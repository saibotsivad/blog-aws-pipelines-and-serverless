const test = require('tape')
const { api } = require('./index.js')

test('simple demo code to show testing', async t => {
	const response = await api()
	t.equal(response.statusCode, '200', 'correct HTTP status is returned')
	t.end()
})
